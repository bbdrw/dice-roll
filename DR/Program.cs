﻿using System;

namespace Dice_Roll
{
    class Program
    {
        static void Main(string[] args)
        {
           Program program = new Program();
            string [] myStrings = {"Please choose the first number: ", "Please choose the second number"};
            String [] myNums = new String[2];

            for(int i = 0; i < myNums.Length; i++){
                Boolean myBool = true;
                do{
                    myBool = true;                    
                    Console.Write(myStrings[i]);
                    myNums[i] = Console.ReadLine();

                    if(!int.TryParse(myNums[i], out int n)){
                        myBool = false;
                        Console.WriteLine();
                        Console.WriteLine("Please enter a valid number!");
                        Console.WriteLine();

                    }              
                }while(myBool == false);
            }

            int randomNum = program.RandomNum();

            Console.WriteLine("Your randomNumber is: " + randomNum);
        }


        public int RandomNum(){

            Random rnd = new Random();
            int num = rnd.Next(1,12);

            return num;
        }
    }
}
